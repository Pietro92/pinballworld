using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper : MonoBehaviour
{
    [SerializeField] private LayerMask _ballMask;
    [SerializeField] private float _explosionForce = 2.5f;

    private void OnCollisionEnter(Collision collision)
    {
        int layer = collision.gameObject.layer;
        if (_ballMask == (_ballMask | (1 << layer)))
        {
            Ball ball = collision.gameObject.GetComponent<Ball>();
            if (ball)
            {
                foreach (ContactPoint contact in collision.contacts)
                {
                    contact.otherCollider.attachedRigidbody.AddForce(-1 * contact.normal * _explosionForce, ForceMode.Impulse);
                }
            }
        }
    }
}
