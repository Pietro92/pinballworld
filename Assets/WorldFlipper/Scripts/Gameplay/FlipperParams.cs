using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/Flipper")]
public class FlipperParams : ScriptableObject
{
    [SerializeField] private float _flipperRestPosition = 0;
    [SerializeField] private float _flipperPressedPosition = 45;
    [SerializeField] private float _flipperHitStrenght = 10000;
    [SerializeField] private float _flipperDamper = 150;
    [SerializeField] private int _direction = 1;

    #region PROPERTY
    public float FlipperRestPosition => _flipperRestPosition;
    public float FlipperPressedPosition => _flipperPressedPosition;
    public float FlipperHitStrenght => _flipperHitStrenght;
    public float FlipperDamper => _flipperDamper;

    public int Direction => _direction;
    #endregion
}
