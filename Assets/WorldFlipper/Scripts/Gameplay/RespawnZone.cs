using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RespawnZone : MonoBehaviour
{
    [Header("Physic Variables")]
    [SerializeField] private LayerMask _ballMask;

    private void OnCollisionEnter(Collision collision)
    {
        int layer = collision.gameObject.layer;
        if (_ballMask == (_ballMask | (1 << layer)))
        {
            collision.gameObject.SetActive(false);
            GlobalEventSystem.Instance.Respawn();
        }
    }
}
