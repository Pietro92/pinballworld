using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAnimatedObject
{
    public void OnActiveGlobalEvent();

    public void OnActiveSpecialEvent();
}
