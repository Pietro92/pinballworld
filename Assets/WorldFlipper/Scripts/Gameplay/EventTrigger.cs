using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

[Serializable]
public class TriggerEvents : UnityEvent { }

public class EventTrigger : MonoBehaviour
{
    [Header("Events variables")]
    [SerializeField] private TriggerEvents _triggerEvent;

    [Header("Physic variables")]
    [SerializeField] private LayerMask _ballMask;

    private void OnTriggerEnter(Collider other)
    {
        int layer = other.gameObject.layer;
        if (_ballMask == (_ballMask | (1 << layer)))
        {
            Ball ball = other.gameObject.GetComponent<Ball>();
            if (ball)
            {
                _triggerEvent?.Invoke();
            }
        }
    }
}
