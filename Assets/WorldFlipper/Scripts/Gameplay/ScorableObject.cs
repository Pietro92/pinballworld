using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ScorableObject : MonoBehaviour
{
    [Header("Score setting")]
    [SerializeField] protected LayerMask _ballMask;
    [SerializeField] protected int _point;

    protected void AddScore()
    {
        GlobalEventSystem.Instance.UpdateScore(_point);
    }
}
