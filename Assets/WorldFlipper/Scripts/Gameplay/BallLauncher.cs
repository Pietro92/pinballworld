using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallLauncher : MonoBehaviour
{
    [Header("Components")]
    [SerializeField] private GameObject _cylinder;
    [SerializeField] private Animator _animator;

    [Header("Physic Variables")]
    [SerializeField] private float _throw;
    [SerializeField] private LayerMask _ballLayer;
    [SerializeField] private Vector3 _collisionBoxSize;

    [Header("Sound Params")]
    [SerializeField] private string _soundPath;
    [SerializeField] private float _helperPosition = 1;

    private void Start()
    {
        GlobalEventSystem.Instance.OnLaunch += LaunchAnimation;
    }

    private void LaunchAnimation()
    {
        _animator.Play("Launch");
    }

    private void Launch()
    {
        //RaycastHit[] hits = Physics.RaycastAll(_cylinder.transform.position, -transform.right, 2.5f, _ballLayer);
        Collider[] colliders = Physics.OverlapBox(_cylinder.transform.position + (-transform.right * 1.2f), _collisionBoxSize, Quaternion.identity, _ballLayer);
        if(colliders.Length > 0)
        {
            Ball _ball = colliders[0].GetComponent<Ball>(); 
            if(_ball)
            {
                foreach (Collider contact in colliders)
                {
                    contact.attachedRigidbody.AddForce(-transform.right * _throw, ForceMode.Impulse);
                }

                // Play sound
                AudioManager.Instance.PlayOneShoot(_soundPath, transform.position + (transform.right * _helperPosition));
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(_cylinder.transform.position + (-transform.right * 1.2f), _collisionBoxSize);
    }
}