using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rubik : MonoBehaviour, IAnimatedObject
{
    [Header("Animation Components")]
    [SerializeField] private Animator _anim;

    private void Start()
    {
        GlobalEventSystem.Instance.OnSpecialEvent += OnActiveSpecialEvent;
    }

    public void OnActiveGlobalEvent()
    {
        
    }

    public void OnActiveSpecialEvent()
    {
        _anim.Play("SpecialEvent");
    }
}
