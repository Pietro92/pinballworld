using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Multiplier : MonoBehaviour
{
    [Header("Multiplier Components")]
    [SerializeField] private Animator _multiplierAnim;

    private bool _isActive = false;

    #region PROPERTY
    public bool IsActive => _isActive;
    #endregion

    public void ActiveMultiplier()
    {
        _multiplierAnim.Play("Active");
        _isActive = true;
    }
}
