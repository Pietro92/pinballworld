using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [Header("Physic Variables")]
    [SerializeField] private Rigidbody rb;

    #region PROPERTY
    public Rigidbody RB
    { get { return rb; } set { rb = value; } }
    #endregion
}
