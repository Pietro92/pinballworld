using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : ScorableObject
{
    private void OnCollisionEnter(Collision collision)
    {
        int layer = collision.gameObject.layer;
        if (_ballMask == (_ballMask | (1 << layer)))
        {
            Ball ball = collision.gameObject.GetComponent<Ball>();
            if (ball)
            {
                // Update score with current object multiplier
                AddScore();
            }
        }
    }
}
