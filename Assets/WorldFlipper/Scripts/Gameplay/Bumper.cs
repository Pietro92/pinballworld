using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Bumper : ScorableObject
{
    [Header("Physic Variables")]
    [SerializeField] private float _explosionForce = 5;

    [Header("Light Variables")]
    [SerializeField] private GameObject _light;

    [Header("Sound Params")]
    [SerializeField] private string _soundPath;
    [SerializeField] private float _helperPosition = 1;

    private void OnCollisionEnter(Collision collision)
    {
        int layer = collision.gameObject.layer;
        if(_ballMask == (_ballMask | (1 << layer)))
        {
            Ball ball = collision.gameObject.GetComponent<Ball>();
            if(ball)
            {
                foreach (ContactPoint contact in collision.contacts)
                {
                    contact.otherCollider.attachedRigidbody.AddForce(-1 * contact.normal * _explosionForce, ForceMode.Impulse);
                }

                // Play sound
                AudioManager.Instance.PlayOneShoot(_soundPath, transform.position + (transform.right * _helperPosition));

                // Active light effect
                StartCoroutine(IEActiveLightEffect());

                // Update score with current object multiplier
                AddScore();
            }
        }
    }

    private IEnumerator IEActiveLightEffect()
    {
        _light.SetActive(true);
        yield return new WaitForSeconds(.1f);
        _light.SetActive(false);
        yield return new WaitForSeconds(.1f);
        _light.SetActive(true);
        yield return new WaitForSeconds(.1f);
        _light.SetActive(false);
    }
}
