using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ramp : MonoBehaviour
{
    [SerializeField] private float thrust = 10;

    private void OnCollisionStay(Collision collision)
    {
        Ball ball = collision.gameObject.GetComponent<Ball>();
        if(ball)
        {
            ball.RB.AddForce(ball.transform.forward * thrust, ForceMode.VelocityChange);
        }
    }
}
