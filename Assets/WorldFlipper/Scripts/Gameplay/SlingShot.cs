using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlingShot : ScorableObject
{
    [Header("Physic Variables")]
    [SerializeField] private float _explosionForce = 5;

    [Header("Light Variables")]
    [SerializeField] private GameObject _light;

    private void OnCollisionEnter(Collision collision)
    {
        int layer = collision.gameObject.layer;
        if (_ballMask == (_ballMask | (1 << layer)))
        {
            Ball ball = collision.gameObject.GetComponent<Ball>();
            if (ball)
            {
                foreach (ContactPoint contact in collision.contacts)
                {
                    contact.otherCollider.attachedRigidbody.AddForce(-1 * contact.normal * _explosionForce, ForceMode.Impulse);
                }

                // Active light effect
                StartCoroutine(IEActiveLightEffect());

                // Update score with current object multiplier
                AddScore();
            }
        }
    }

    private IEnumerator IEActiveLightEffect()
    {
        _light.SetActive(true);
        yield return new WaitForSeconds(.1f);
        _light.SetActive(false);
        yield return new WaitForSeconds(.1f);
        _light.SetActive(true);
        yield return new WaitForSeconds(.1f);
        _light.SetActive(false);
    }
}
