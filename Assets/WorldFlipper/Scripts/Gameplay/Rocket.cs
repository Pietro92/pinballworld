using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour, IAnimatedObject
{
    [SerializeField] private Transform _spawnPoint;

    public void OnActiveGlobalEvent()
    {
        GlobalEventSystem.Instance.SpawnBallAtPosition(_spawnPoint.position);
    }

    public void OnActiveSpecialEvent()
    {
        
    }
}
