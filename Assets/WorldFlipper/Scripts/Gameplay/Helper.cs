using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helper : MonoBehaviour
{
    [Header("Physic Variables")]
    [SerializeField] private float _explosionForce = 5;
    [SerializeField] private LayerMask _ballMask;
    [SerializeField] private Vector3 _versorHelper = Vector3.up;

    [Header("Sound Params")]
    [SerializeField] private string _soundPath;

    private void OnCollisionEnter(Collision collision)
    {
        int layer = collision.gameObject.layer;
        if (_ballMask == (_ballMask | (1 << layer)))
        {
            Ball ball = collision.gameObject.GetComponent<Ball>();
            bool bCheck = CheckCollision(collision);
            if (ball && bCheck)
            {
                foreach (ContactPoint contact in collision.contacts)
                {
                    contact.otherCollider.attachedRigidbody.AddForce(-1 * contact.normal * _explosionForce, ForceMode.Impulse);
                }

                // Play sound
                AudioManager.Instance.PlayOneShoot(_soundPath);
            }
        }
    }

    private bool CheckCollision(Collision col)
    {
        Vector3 DistanceNorm = (transform.position - col.transform.position).normalized;
        if(Vector3.Dot(DistanceNorm, _versorHelper) > 0)
        {
            return true;
        }
        return false;
    }
}
