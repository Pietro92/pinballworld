using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SceneDebugger : MonoBehaviour
{
    [Header("Main Debugger Components")]
    [SerializeField] private GameManager _gameManager;
    [SerializeField] private UIManager _uiManager;
    [SerializeField] private Ball _ball;

    [Header("Panel Components")]
    [SerializeField] private GameObject _mainPanel;

    [Header("Text Components")]
    [SerializeField] private TextMeshProUGUI _ballLocationText;
    [SerializeField] private TextMeshProUGUI _multiplierText;
    [SerializeField] private TextMeshProUGUI _multiplierCounterIndexText;
    [SerializeField] private TextMeshProUGUI _timerText;

    [Header("Components")]
    [SerializeField] private bool _activeDebugger;

    private void Awake()
    {
        
    }

    private void Start()
    {
        _mainPanel.SetActive(_activeDebugger);
    }

    private void FixedUpdate()
    {
        
    }

    private void Update()
    {
        if(_activeDebugger)
        {
            _ballLocationText.text = string.Format("Ball Location: {0}", _ball.transform.position);
            _multiplierText.text = string.Format("Multiplier: {0}", _gameManager.Multipler);
            _multiplierCounterIndexText.text = string.Format("MultiplierNextIndex: {0}", _gameManager.MultiplierCounterIndex);
            _timerText.text = string.Format("MultiplierTimer: {0}", _gameManager.CounterTimer);
        }
    }

    private void LateUpdate()
    {
        
    }
}
