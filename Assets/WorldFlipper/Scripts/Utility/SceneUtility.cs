using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum SceneIndex
{
    Menu,
    Easy,
    Medium,
    Hard,
}

public class SceneUtility
{
    static public IEnumerator ImageFideIn(Image sprite, Color color, float fadeTime)
    {
        float waitTime = 0;
        while (waitTime < 1)
        {
            sprite.color = Color.Lerp(Color.clear, color, waitTime);
            yield return null;
            waitTime += Time.deltaTime / fadeTime;
        }
    }

    static public IEnumerator ImageFideOut(Image sprite, Color color, float fadeTime)
    {
        float waitTime = 0;
        while (waitTime < 1)
        {
            sprite.color = Color.Lerp(color, Color.clear, waitTime);
            yield return null;
            waitTime += Time.deltaTime / fadeTime;
        }
    }

    static public IEnumerator TextFideIn(TextMeshProUGUI text, Color color, float fadeTime)
    {
        float waitTime = 0;
        while (waitTime < 1)
        {
            text.color = Color.Lerp(Color.clear, color, waitTime);
            yield return null;
            waitTime += Time.deltaTime / fadeTime;
        }
    }

    static public IEnumerator TextFideOut(TextMeshProUGUI text, Color color, float fadeTime)
    {
        float waitTime = 0;
        while (waitTime < 1)
        {
            text.color = Color.Lerp(color, Color.clear, waitTime);
            yield return null;
            waitTime += Time.deltaTime / fadeTime;
        }
    }

    static public IEnumerator SoundFideIn(FMODUnity.StudioEventEmitter emitter, float fadeTime)
    {
        float waitTime = 0;
        while (waitTime < 1)
        {
            emitter.SetParameter("Fade", Mathf.Lerp(0, 1, waitTime));
            yield return null;
            waitTime += Time.deltaTime / fadeTime;
        }
    }

    static public IEnumerator SoundFideOut(FMODUnity.StudioEventEmitter emitter, float fadeTime)
    {
        float waitTime = 0;
        while (waitTime < 1)
        {
            emitter.SetParameter("Fade", Mathf.Lerp(1, 0, waitTime));
            yield return null;
            waitTime += Time.deltaTime / fadeTime;
        }
    }

    static public void ScoreSort(ref List<long> Scores)
    {
        for(int i = 0; i < Scores.Count; i++)
        {
            for(int j = i + 1; j < Scores.Count; j++)
            {
                if(Scores[i] < Scores[j])
                {
                    long tmp = Scores[i];
                    Scores[i] = Scores[j];
                    Scores[j] = tmp;
                }
            }
        }
    }
}
