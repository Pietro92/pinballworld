using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Storage/Audio")]
public class AudioStorage : ScriptableObject
{
    [SerializeField] private int _master;
    [SerializeField] private int _music;
    [SerializeField] private int _sfx;

    #region PROPERTY
    public int Master
    {
        get { return _master; }
        set { _master = value; }
    }
    public int Music
    {
        get { return _music; }
        set { _music = value; }
    }
    public int SFX
    {
        get { return _sfx; }
        set { _sfx = value; }
    }
    #endregion
}
