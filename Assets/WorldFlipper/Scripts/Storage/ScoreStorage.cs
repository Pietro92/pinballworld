using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Storage/Score")]
public class ScoreStorage : ScriptableObject
{
    [SerializeField] private List<long> _easyScores;
    [SerializeField] private List<long> _mediumScores;
    [SerializeField] private List<long> _hardScores;

    public void UpdateScore(ref List<long> scores, long score)
    {
        if (scores.Count < 8)
        {
            // Check don't repeat same scores
            for(int i = 0; i < scores.Count; i++)
            {
                if (scores[i] == score)
                    return;
            }

            // Add score and update score's list with sort
            scores.Add(score);
            SceneUtility.ScoreSort(ref scores);
            return;
        }

        // If score's list has 8 elements
        for (int i = 0; i < scores.Count; i++)
        {
            if (scores[i] < score)
            {
                scores[i] = score;
                return;
            }
        }
    }

    #region PROPERTY
    public List<long> EasyScores => _easyScores;
    public List<long> MediumScores => _mediumScores;
    public List<long> HardScores => _hardScores;
    #endregion
}
