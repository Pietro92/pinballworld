using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class GameManager : MonoBehaviour
{
    [Header("Managers")]
    [SerializeField] private HUDManager _hudManager;

    [Header("Camera Components")]
    [SerializeField] private Camera _secondaryCamera;

    [Header("Pinball Components")]
    [SerializeField] private Ball _ball;

    [Header("Multiplier Components")]
    [SerializeField] private bool _isMultiplierMode = false;
    [ConditionalField("_isMultiplierMode")][SerializeField] private Multiplier _multiplier1;
    [ConditionalField("_isMultiplierMode")][SerializeField] private Multiplier _multiplier2;
    [ConditionalField("_isMultiplierMode")][SerializeField] private Multiplier _multiplier3;
    [ConditionalField("_isMultiplierMode")][SerializeField] private int _timerRange = 120;
    [ConditionalField("_isMultiplierMode")][SerializeField] private int _numberOfActiveMultipliersForSpecialEvent = 3;

    [Header("Audio")]
    [SerializeField] private FMODUnity.StudioEventEmitter _mainEmitter;

    [Header("Utility Variables")]
    [SerializeField] private int _ballsCount = 3;

    // Private variables
    private Vector3 _startPosition = Vector3.zero;
    private long _score = 0;
    private int _multiplierGame = 1;
    private int _counterNextMultiplier = 0;
    private Coroutine _multiplierReset;
    private int _counterTimer = 0;
    private int _numberOfBallsInScene = 1;

    #region PROPERTY
    public int Multipler => _multiplierGame;
    public int MultiplierCounterIndex => _counterNextMultiplier;
    public int CounterTimer => _counterTimer;
    #endregion

    private void Start()
    {
        //Initialize ball's start position
        // Utility when the ball re-spawn
        _startPosition = _ball.transform.position;

        // Initialize all delegate events
        InitDelegates();
    }

    #region Init
    private void InitDelegates()
    {
        // Binding
        GlobalEventSystem.Instance.OnSwitchCamera += SwitchCamera;
        GlobalEventSystem.Instance.OnRespawn += Respawn;
        GlobalEventSystem.Instance.OnSpawnBallAtPosition += SpawnBallAtPosition;
        GlobalEventSystem.Instance.OnUpdateScore += UpdateScore;
        GlobalEventSystem.Instance.OnLoad += LoadingComplete;

        // Broadcast
        GlobalEventSystem.Instance.InitBallsCount(_ballsCount);
        GlobalEventSystem.Instance.UpdateNumberOfBallsInScene(_numberOfBallsInScene);
    }
    #endregion

    #region Update Params
    private void UpdateScore(int value)
    {
        _score = Mathf.RoundToInt(Mathf.Clamp(_score + (value * _multiplierGame), 0, 999999999));
        _hudManager.UpdateTextScore(_score);

        // Multiplier check
        if(_isMultiplierMode)
        {
            UpdateMultiplier();
        }
    }

    private void UpdateMultiplier()
    {
        _counterNextMultiplier++;

        if(_counterNextMultiplier < 10)
        {
            return;
        }

        // Update multiplier index and counter index
        // Apply multiplier animation
        _counterNextMultiplier = 0;
        _multiplierGame = Mathf.Clamp(++_multiplierGame, 1, 3);

        // Active multiplier animation
        ActiveMultipliers();

        // Active special event in scene
        // Not all levels contain the special event, but the global event manager broad that event
        if (_multiplierGame == _numberOfActiveMultipliersForSpecialEvent)
        {
            GlobalEventSystem.Instance.ActiveSpecialEvent();
        }

        // Coroutine control
        if (_multiplierReset != null)
        {
            StopCoroutine(_multiplierReset);
        }
        _multiplierReset = StartCoroutine(IEMultiplierTimer());
    }

    private void ActiveMultipliers()
    {
        switch(_multiplierGame)
        {
            case 2:
                if (!_multiplier2.IsActive)
                {
                    _multiplier2.ActiveMultiplier();
                }
                break;
            case 3:
                if(!_multiplier3.IsActive)
                {
                    _multiplier3.ActiveMultiplier();
                }
                break;
            default:
                break;
        }
    }
    #endregion

    #region Update Status
    private void SwitchCamera()
    {
        _secondaryCamera.gameObject.SetActive(!_secondaryCamera.gameObject.activeSelf);
    }

    private void Respawn()
    {
        if (_numberOfBallsInScene > 1)
        {
            _numberOfBallsInScene--;
            GlobalEventSystem.Instance.UpdateNumberOfBallsInScene(_numberOfBallsInScene);
            return;
        }

        // Update balls
        _ballsCount--;
        _numberOfBallsInScene = 1;
        GlobalEventSystem.Instance.UpdateNumberOfBallsInScene(_numberOfBallsInScene);

        // Update number of playable balls and start re-spawn timer
        _hudManager.UpdateTextBallCount(_ballsCount);

        if (_ballsCount <= 0)
        {
            GlobalEventSystem.Instance.GameOver(_score);
            return;
        }
        StartCoroutine(IERespawnTimer());
    }

    private void SpawnBallAtPosition(Vector3 position)
    {
        _ball.transform.position = position;
    }

    private void LoadingComplete()
    {
        StartCoroutine(AudioManager.Instance.PlayLoading(_mainEmitter));
    }

    public void SpawnBalls()
    {
        if(_numberOfBallsInScene == 1)
        {
            Instantiate(_ball.gameObject, _ball.transform.position, Quaternion.identity, _ball.transform.parent);
            Instantiate(_ball.gameObject, _ball.transform.position, Quaternion.identity, _ball.transform.parent);
            _numberOfBallsInScene += 2;
            GlobalEventSystem.Instance.UpdateNumberOfBallsInScene(_numberOfBallsInScene);
        }
    }
    #endregion

    #region Coroutine
    private IEnumerator IERespawnTimer()
    {
        yield return new WaitForSeconds(3f);
        _ball.transform.position = _startPosition;
        _ball.RB.velocity = Vector3.zero;
        _ball.gameObject.SetActive(true);
    }

    private IEnumerator IEMultiplierTimer()
    {
        while(_counterTimer < _timerRange)
        {
            _counterTimer++;
            yield return new WaitForSecondsRealtime(1);
        }
        _multiplierGame = 1;
        _counterNextMultiplier = 0;
    }
    #endregion
}
