using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using FMOD.Studio;

public class AudioManager : Singleton<AudioManager>
{
    private VCA mMasterMixer;
    private VCA mMusicMixer;
    private VCA mSfxMixer;

    private float mMasterMixerVolume;
    public float MasterMixerVolume => mMasterMixerVolume;

    private float mMusicMixerVolume;
    public float MusicMixerVolume => mMusicMixerVolume;

    private float mSfxMixerVolume;
    public float SfxMixerVolume => mSfxMixerVolume;

    private void Awake()
    {
        mMasterMixer = RuntimeManager.GetVCA("vca:/Master");
        mMusicMixer = RuntimeManager.GetVCA("vca:/Music");
        mSfxMixer = RuntimeManager.GetVCA("vca:/SFX");
    }

    private void Start()
    {
        GlobalEventSystem.Instance.OnUpdateMasterMixerVolume += UpdateMasterMixerVolume;
        GlobalEventSystem.Instance.OnUpdateMusicMixerVolume += UpdateMusicMixerVolume;
        GlobalEventSystem.Instance.OnUpdateSfxMixerVolume += UpdateSfxMixerVolume;
    }

    #region Mixer
    public void UpdateMasterMixerVolume(float volume)
    {
        mMasterMixerVolume = volume;
        mMasterMixer.setVolume(mMasterMixerVolume);
    }

    public void UpdateMusicMixerVolume(float volume)
    {
        mMusicMixerVolume = volume;
        mMusicMixer.setVolume(mMusicMixerVolume);
    }

    public void UpdateSfxMixerVolume(float volume)
    {
        mSfxMixerVolume = volume;
        mSfxMixer.setVolume(mSfxMixerVolume);
    }
    #endregion

    #region Play Sound
    public void PlayOneShoot(string soundPath, Vector3 position = default(Vector3))
    {
        RuntimeManager.PlayOneShot(soundPath, position);
    }
    #endregion

    #region Coroutine
    public IEnumerator PlayLoading(StudioEventEmitter emitter)
    {
        emitter.Play();
        yield return StartCoroutine(SceneUtility.SoundFideIn(emitter, 0.6f));
    }
    #endregion
}
