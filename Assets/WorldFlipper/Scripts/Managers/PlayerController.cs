using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Flipper Variables")]
    [SerializeField] private HingeJoint _flipperLJoint;
    [SerializeField] private HingeJoint _flipperRJoint;
    [SerializeField] private FlipperParams _flipperLParameters;
    [SerializeField] private FlipperParams _flipperRParameters;

    [Header("Sound Params")]
    [SerializeField] private string _soundPath;

    // Private variables
    private JointSpring _springLeft;
    private JointSpring _springRight;

    private bool _triggerA = false;
    private bool _triggerD = false;

    private bool _isGameover = false;
    private bool _isLoading = true;

    private void Start()
    {
        InitDelegates();
        InitPhysicVariables();
    }

    private void Update()
    {
        if(!_isGameover && !_isLoading)
        {
            _triggerA = Input.GetKey(KeyCode.A);
            _triggerD = Input.GetKey(KeyCode.D);

            if (Input.GetKeyDown(KeyCode.A))
            {
                AudioManager.Instance.PlayOneShoot(_soundPath, _flipperLJoint.gameObject.transform.position);
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                AudioManager.Instance.PlayOneShoot(_soundPath, _flipperRJoint.gameObject.transform.position);
            }
            if(Input.GetKeyDown(KeyCode.Space))
            {
                GlobalEventSystem.Instance.Launch();
            }
        }    
    }

    private void FixedUpdate()
    {
        if(!_isGameover && !_isLoading)
        {
            UpdateJointParams(_triggerA, ref _flipperLJoint, ref _springLeft, _flipperLParameters);
            UpdateJointParams(_triggerD, ref _flipperRJoint, ref _springRight, _flipperRParameters);
        }
    }

    #region Init
    private void InitDelegates()
    {
        GlobalEventSystem.Instance.OnGameOver += GameOver;
        GlobalEventSystem.Instance.OnLoad += DisableLoading;
    }

    private void InitPhysicVariables()
    {
        InitJoint(ref _flipperLJoint, ref _springLeft, _flipperLParameters);
        InitJoint(ref _flipperRJoint, ref _springRight, _flipperRParameters);
    }

    private void InitJoint(ref HingeJoint hingeJoint, ref JointSpring springJoint, FlipperParams flipperParams)
    {
        hingeJoint.useSpring = true;

        springJoint = new JointSpring();
        springJoint.spring = flipperParams.FlipperHitStrenght;
        springJoint.damper = flipperParams.FlipperDamper;
        hingeJoint.useLimits = true;

        JointLimits limitsJoint = new JointLimits();
        limitsJoint.min = flipperParams.FlipperRestPosition;
        limitsJoint.max = flipperParams.FlipperPressedPosition * flipperParams.Direction;
        hingeJoint.limits = limitsJoint;
    }
    #endregion

    #region Update Game Params
    private void UpdateJointParams(bool bTrigger, ref HingeJoint hingeJoint, ref JointSpring spingJoint, FlipperParams flipperParams)
    {
        if (bTrigger)
        {
            spingJoint.targetPosition = flipperParams.FlipperPressedPosition * flipperParams.Direction;
        }
        else
        {
            spingJoint.targetPosition = flipperParams.FlipperRestPosition;
        }

        // Update physic property
        hingeJoint.spring = spingJoint;
    }
    #endregion

    #region Update Game Status
    private void GameOver(long score)
    {
        _isGameover = true;
    }

    private void DisableLoading()
    {
        _isLoading = false;
    }
    #endregion
}
