using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Sound")]
public class AudioParams : ScriptableObject
{
    [SerializeField] private string[] _sounds;

    #region PROPERTY
    public string[] Sounds => _sounds;
    #endregion
}
