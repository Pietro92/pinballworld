using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GlobalEventSystem : Singleton<GlobalEventSystem>
{
    // Counters events
    public event Action<int> OnUpdateScore;
    public event Action<int> OnInitBallsCount;
    public event Action<int> OnUpdateNumberOfBallsInScene;

    // Game Status events
    public event Action OnSwitchCamera;
    public event Action OnRespawn;
    public event Action<Vector3> OnSpawnBallAtPosition;
    public event Action<long> OnGameOver;
    public event Action OnLoad;
    public event Action OnLaunch;
    public event Action OnSpecialEvent;

    // Sound events
    public event Action<float> OnUpdateMasterMixerVolume;
    public event Action<float> OnUpdateMusicMixerVolume;
    public event Action<float> OnUpdateSfxMixerVolume;

    #region COUNTERS
    public void InitBallsCount(int ballsCount)
    {
        OnInitBallsCount?.Invoke(ballsCount);
    }

    public void UpdateScore(int value)
    {
        OnUpdateScore?.Invoke(value);
    }

    public void UpdateNumberOfBallsInScene(int value)
    {
        OnUpdateNumberOfBallsInScene?.Invoke(value);
    }
    #endregion

    #region GAME STATUS
    public void SwitchCamera()
    {
        OnSwitchCamera?.Invoke();
    }

    public void Respawn()
    {
        OnRespawn?.Invoke();
    }

    public void SpawnBallAtPosition(Vector3 position)
    {
        OnSpawnBallAtPosition?.Invoke(position);
    }

    public void GameOver(long score)
    {
        OnGameOver?.Invoke(score);
    }

    public void LoadingComplete()
    {
        OnLoad?.Invoke();
    }

    public void Launch()
    {
        OnLaunch?.Invoke();
    }

    public void ActiveSpecialEvent()
    {
        OnSpecialEvent?.Invoke();
    }
    #endregion

    #region SOUNDS
    public void UpdateMasterMixerVolume(float volume)
    {
        OnUpdateMasterMixerVolume?.Invoke(volume);
    }

    public void UpdateMusicMixerVolume(float volume)
    {
        OnUpdateMusicMixerVolume?.Invoke(volume);
    }

    public void UpdateSfxMixerVolume(float volume)
    {
        OnUpdateSfxMixerVolume?.Invoke(volume);
    }
    #endregion
}
