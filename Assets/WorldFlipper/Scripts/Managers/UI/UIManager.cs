using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class UIManager : MonoBehaviour
{
    [Header("Panels Params")]
    [SerializeField] protected GameObject _mainPanel;
    [SerializeField] protected GameObject _transictionPanel;

    [Header("Transition Variables")]
    [SerializeField] protected Image _transitionImage;
    [SerializeField] protected GameObject _sprites;

    [Header("Sounds Path")]
    [SerializeField] private FMODUnity.StudioEventEmitter _mainEmitter;

    [Header("Storage Components")]
    [SerializeField] protected ScoreStorage _scoreStorage;

    virtual protected IEnumerator IEStartNewScene(int index)
    {
        yield return StartCoroutine(SceneUtility.ImageFideIn(_transitionImage, Color.white, 0.6f));
        yield return new WaitForSeconds(2);
        _sprites.SetActive(true);
        yield return new WaitForSeconds(5);
        yield return StartCoroutine(SceneUtility.SoundFideOut(_mainEmitter, .6f));
        yield return new WaitForSeconds(3);
        _sprites.SetActive(false);
        yield return new WaitUntil(() => SceneManager.LoadSceneAsync(index, LoadSceneMode.Single).isDone);
    }
}
