using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class HUDManager : UIManager
{
    [Header("Panels Params")]
    [SerializeField] private GameObject _endPanel;

    [Header("Text Components")]
    [SerializeField] private TextMeshProUGUI _scoreText;
    [SerializeField] private TextMeshProUGUI _ballCountText;
    [SerializeField] private TextMeshProUGUI _congratulationsText;
    [SerializeField] private TextMeshProUGUI _currentScoreText;
    [SerializeField] private TextMeshProUGUI _bestScoreText;

    [Header("Animation Components")]
    [SerializeField] private Animator _blackboardAnim;

    // Private Variables
    private int _initBallsCount = 0;
    private int _numberOfBallsInScene = 1;
    private SceneIndex _sceneIndex = 0;

    private void Awake()
    {
        InitDelegates();
    }

    private void Start()
    {
        // Init current scene index
        // This variable is utile for update scores
        _sceneIndex = (SceneIndex)SceneManager.GetActiveScene().buildIndex;

        // Initialize main text with default values
        InitText();

        // Start basic blackboard animation
        StartCoroutine(IELoading());
    }

    #region Init
    private void InitDelegates()
    {
        GlobalEventSystem.Instance.OnInitBallsCount += InitBallsCount;
        GlobalEventSystem.Instance.OnUpdateNumberOfBallsInScene += UpdateNumberOfBallsInScene;
        GlobalEventSystem.Instance.OnGameOver += GameOver;
        GlobalEventSystem.Instance.OnRespawn += Respawn;
    }

    private void InitText()
    {
        _scoreText.text = "0";
        _ballCountText.text = _initBallsCount.ToString();
    }

    private void InitBallsCount(int ballsCount)
    {
        _initBallsCount = ballsCount;
    }  
    #endregion

    #region Update Game Params
    private void UpdateScore(long score)
    {
        switch (_sceneIndex)
        {
            case SceneIndex.Easy:
                UpdateScoreStorage(_scoreStorage.EasyScores, score);
                UpdateScoresText(_scoreStorage.EasyScores, score);
                break;
            case SceneIndex.Medium:
                UpdateScoreStorage(_scoreStorage.MediumScores, score);
                UpdateScoresText(_scoreStorage.MediumScores, score);
                break;
            case SceneIndex.Hard:
                UpdateScoreStorage(_scoreStorage.HardScores, score);
                UpdateScoresText(_scoreStorage.HardScores, score);
                break;
            case 0:
                Debug.LogWarning("Scene index error!");
                break;
        }
    }

    private void UpdateScoreStorage(List<long> scores, long score)
    {
        _scoreStorage.UpdateScore(ref scores, score);
    }

    private void UpdateScoresText(List<long> scores, long score)
    {

        _bestScoreText.text = string.Format("Best Score : {0}", scores[0]);
        _currentScoreText.text = string.Format("Current Score : {0}", score);
    }

    private void UpdateNumberOfBallsInScene(int value)
    {
        _numberOfBallsInScene = value;
    }    
    #endregion

    #region Update Game Status
    private void GameOver(long score)
    {
        StartCoroutine(IEGameOver(score));
    }

    private void Respawn()
    {
        if(_numberOfBallsInScene > 1)
        {
            return;
        }

        StartCoroutine(IERespawn());
    }
    #endregion

    #region Coroutine
    private IEnumerator IEGameOver(long score)
    {
        // Update the score storage and print the best and current scores
        UpdateScore(score);

        // Open end panel and fade the texts
        yield return StartCoroutine(IECongratulations());

        // Active the transition 
        yield return new WaitForSeconds(5);
        _endPanel.SetActive(false);
        _transictionPanel.SetActive(true);
        yield return StartCoroutine(IEStartNewScene((int)SceneIndex.Menu));
    }

    private IEnumerator IECongratulations()
    {
        _mainPanel.SetActive(false);
        _endPanel.SetActive(true);
        yield return StartCoroutine(SceneUtility.TextFideIn(_congratulationsText, Color.white, .6f));
        yield return StartCoroutine(SceneUtility.TextFideIn(_currentScoreText, Color.red, .6f));
        yield return StartCoroutine(SceneUtility.TextFideIn(_bestScoreText, Color.green, .6f));
    }

    public IEnumerator IEStartBlackboardAnimation()
    {
        _blackboardAnim.Play("ViewBall");
        yield return new WaitForSeconds(4);
        _blackboardAnim.Play("ViewScore");
    }

    private IEnumerator IELoading()
    {
        yield return new WaitForSeconds(2);
        yield return StartCoroutine(SceneUtility.ImageFideOut(_transitionImage, Color.white, 0.5f));
        _transictionPanel.SetActive(false);
        GlobalEventSystem.Instance.LoadingComplete();
        yield return new WaitForSeconds(1);
        yield return StartCoroutine(IEStartBlackboardAnimation());
    }

    private IEnumerator IERespawn()
    {
        _blackboardAnim.Play("FlipScore");
        yield return new WaitForSeconds(3f);
        yield return StartCoroutine(IEStartBlackboardAnimation());
    }
    #endregion

    #region Callback
    public void UpdateTextScore(long _score)
    {
        _scoreText.text = _score.ToString();
    }

    public void UpdateTextBallCount(int ballsCount)
    {
        _ballCountText.text = ballsCount.ToString();
    }

    public void OnHomeBack()
    {
        GlobalEventSystem.Instance.GameOver(0);
        GameOver(0);
    }

    public void OnSwitchCamera()
    {
        GlobalEventSystem.Instance.SwitchCamera();
    }    
    #endregion
}
