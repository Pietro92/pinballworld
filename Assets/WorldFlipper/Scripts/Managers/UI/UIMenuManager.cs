using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class UIMenuManager : UIManager
{
    [Header("Panels Params")]
    [SerializeField] private GameObject _levelsPanel;
    [SerializeField] private GameObject _soundPanel;
    [SerializeField] private GameObject _scorePanel;

    [Header("Scores Params")]
    [SerializeField] private List<TextMeshProUGUI> _easyScores;
    [SerializeField] private List<TextMeshProUGUI> _mediumScores;
    [SerializeField] private List<TextMeshProUGUI> _hardScores;

    [Header("Slider Params")]
    [SerializeField] private Slider _masterSlider;
    [SerializeField] private Slider _musicSlider;
    [SerializeField] private Slider _sfxSlider;

    [Header("Sounds Path")]
    [SerializeField] private string _buttonSoundPath;

    [Header("Storage Params")]
    [SerializeField] private AudioStorage _audioStorage;

    // Private variables
    private GameObject _currentPanel;

    private void Awake()
    {
        _currentPanel = _mainPanel;
    }

    private void Start()
    {
        InitAudioParams();
        InitScore(_easyScores, _scoreStorage.EasyScores);
        InitScore(_mediumScores, _scoreStorage.MediumScores);
        InitScore(_hardScores, _scoreStorage.HardScores);
    }

    #region Init
    private void InitAudioParams()
    {
        _masterSlider.value = _audioStorage.Master;
        _musicSlider.value = _audioStorage.Music;
        _sfxSlider.value = _audioStorage.SFX;
    }

    private void InitScore(List<TextMeshProUGUI> selectedScoreList, List<long> scores)
    {
        for(int i = 0; i < selectedScoreList.Count; i++)
        {
            if(i < scores.Count)
            {
                selectedScoreList[i].text = scores[i].ToString();
            }
            else
            {
                selectedScoreList[i].text = "0";
            }
        }
    }
    #endregion

    #region Callback
    public void OnExit()
    {
        Application.Quit();
    }

    #region Open Panel
    public void OnOpenLevelsPanel()
    {
        _currentPanel.SetActive(false);
        _levelsPanel.SetActive(true);
        _currentPanel = _levelsPanel;
    }

    public void OnOpenSoundPanel()
    {
        _currentPanel.SetActive(false);
        _soundPanel.SetActive(true);
        _currentPanel = _soundPanel;
    }

    public void OnOpenScorePanel()
    {
        _currentPanel.SetActive(false);
        _scorePanel.SetActive(true);
        _currentPanel = _scorePanel;
    }

    public void OnBackMainMenu()
    {
        _currentPanel.SetActive(false);
        _mainPanel.SetActive(true);
        _currentPanel = _mainPanel;
    }
#endregion  

    #region Start New Scene
    public void OnStartEasyMode()
    {
        // Active Panel
        _transictionPanel.SetActive(true);

        StartCoroutine(IEStartNewScene((int)SceneIndex.Easy));
    }

    public void OnStartMediumMode()
    {
        // Active Panel
        _transictionPanel.SetActive(true);

        StartCoroutine(IEStartNewScene((int)SceneIndex.Medium));
    }

    public void OnStartHardModex()
    {
        // Active Panel
        _transictionPanel.SetActive(true);

        StartCoroutine(IEStartNewScene((int)SceneIndex.Hard));
    }
    #endregion

    #region Sound
    public void OnPlayButtonSound()
    {
        AudioManager.Instance.PlayOneShoot(_buttonSoundPath);
    }

    public void OnSaveSound()
    {
        _currentPanel.SetActive(false);
        _mainPanel.SetActive(true);
        _currentPanel = _mainPanel;

        // Save params
        _audioStorage.Master = Mathf.RoundToInt(_masterSlider.value);
        _audioStorage.Music = Mathf.RoundToInt(_musicSlider.value);
        _audioStorage.SFX = Mathf.RoundToInt(_sfxSlider.value);
    }

    public void OnUpdateMasterMixerVolume()
    {
        float volume = Mathf.Pow(10.0f, _masterSlider.value / 20f);
        GlobalEventSystem.Instance.UpdateMasterMixerVolume(volume);
    }

    public void OnUpdateMusicMixerVolume()
    {
        float volume = Mathf.Pow(10.0f, _musicSlider.value / 20f);
        GlobalEventSystem.Instance.UpdateMusicMixerVolume(volume);
    }

    public void OnUpdateSfxMixerVolume()
    {
        float volume = Mathf.Pow(10.0f, -_sfxSlider.value / 20f);
        GlobalEventSystem.Instance.UpdateSfxMixerVolume(volume);
    }
    #endregion

    #endregion
}
